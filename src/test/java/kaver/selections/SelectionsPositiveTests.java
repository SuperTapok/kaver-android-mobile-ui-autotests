package kaver.selections;

import io.qameta.allure.*;
import kaver.BaseTest;
import org.junit.jupiter.api.*;

@Epic("UI")
@Feature("Подборки")
@Tag("Позитивный")
public class SelectionsPositiveTests extends BaseTest {
    @Test
    @DisplayName("Фильтрация событий по категориям")
    @Severity(value = SeverityLevel.NORMAL)
    void filterEventsByCategoryTest() {
        selectionPage = mainPage.openFreeSelectionPage();

        Assertions.assertTrue(selectionPage.checkCostOfAllEvents("Бесплатно"));
    }
}

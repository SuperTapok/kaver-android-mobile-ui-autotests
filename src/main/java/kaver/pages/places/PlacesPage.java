package kaver.pages.places;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import kaver.pages.search.SearchPage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.appium.SelenideAppium.$;

public class PlacesPage {
    private final SelenideElement
        artSpacesBtn = $(By.id("Арт-пространства")),
        searchBtn    = $(By.id("search_button"));

    @Step("Нажать на кнопку поиска")
    public SearchPage clickSearchBtn() {
        searchBtn.click();

        return new SearchPage();
    }

    @Step("Нажать на арт-пространства")
    public PlaceCategoryPage clickArtSpacesBtn() {
        artSpacesBtn.click();

        return new PlaceCategoryPage();
    }
}

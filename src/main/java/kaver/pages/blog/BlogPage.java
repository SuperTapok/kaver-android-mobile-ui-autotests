package kaver.pages.blog;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

import static com.codeborne.selenide.appium.SelenideAppium.$x;

public class BlogPage {
    private final SelenideElement firstArticleTitle = $x("(//android.view.ViewGroup" +
            "[@resource-id=\"article\"])[1]/android.view.ViewGroup[1]/android.widget.TextView");

    @Step("Получить название статьи")
    @Attachment(value = "Название")
    public String getFirstArticleTitle() {
        return firstArticleTitle.text();
    }

    @Step("Открыть подробную страницу статьи")
    public ArticlePage clickFirstArticleTitle() {
        firstArticleTitle.click();

        return new ArticlePage();
    }
}

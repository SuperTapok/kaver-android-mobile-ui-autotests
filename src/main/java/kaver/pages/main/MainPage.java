package kaver.pages.main;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import kaver.elements.NavbarElement;
import kaver.pages.blog.BlogPage;
import kaver.pages.selection.SelectionPage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.appium.SelenideAppium.$;

public class MainPage {
    public NavbarElement navbarElement = new NavbarElement();

    private final SelenideElement
        blogBtn          = $(By.id("button")),
        freeSelectionBtn = $x("(//android.view.ViewGroup[@resource-id=\"Бесплатно\"])[1]");

    @Step("Открыть бесплатную подборку")
    public SelectionPage openFreeSelectionPage(){
        freeSelectionBtn.scrollTo().click();

        return new SelectionPage();
    }

    @Step("Открыть блог")
    public BlogPage clickBlogBtn() {
        blogBtn.scrollTo().click();

        return new BlogPage();
    }
}

package kaver.pages.bookmarks;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.appium.SelenideAppium.$;

public class EventsBookmarksPage {
    private final SelenideElement
        firstEventAddToBookmarkBtn = $(By.id("favorite_button")),
        firstEventTitle            = $(By.id("event_content"));

    @Step("Название первого события совпадает с {title}?")
    public boolean checkFirstEventTitleEqualsValue(String title) {
        return firstEventTitle.text().equals(title);
    }

    @Step("Убрать событие из избранного")
    public void removeFirstFromFavourites() {
        firstEventAddToBookmarkBtn.click();
        firstEventAddToBookmarkBtn.shouldNotBe(exist);
    }
}

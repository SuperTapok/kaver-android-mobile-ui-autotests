package kaver.pages.auth;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import kaver.helpers.RandomPagesHelper;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.interactable;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.appium.SelenideAppium.$;

public class ChooseAccountPage {
    private final SelenideElement
        addAccountBtn             = $(By.id("info.goodline.events:id/btnPassportAddAccount")),
        confirmAccountDeletionBtn = $(By.id("android:id/button1")),
        deleteAccountBtn          = $(By.id("info.goodline.events:id/btnGlAccountDelete")),
        loggedInAccount           = $(By.id("info.goodline.events:id/select_item_account"));

    private ChooseAccountPage clickDeleteAccountBtn() {
        deleteAccountBtn.click();

        return this;
    }

    private ChooseAccountPage clickConfirmDeletionBtn() {
        confirmAccountDeletionBtn.click();

        return this;
    }

    private AuthByPhonePage clickAddAccountBtn() {
        addAccountBtn.shouldBe(interactable).click();

        return new AuthByPhonePage();
    }

    @Step("Добавить новый аккаунт")
    public AuthByPhonePage deleteExistingAndAddNewAccount() {
        if (RandomPagesHelper.isElementExist(loggedInAccount, visible)) {
            return clickDeleteAccountBtn()
                .clickConfirmDeletionBtn()
                .clickAddAccountBtn();
        }
        else{
            return new AuthByPhonePage();
        }
    }
}

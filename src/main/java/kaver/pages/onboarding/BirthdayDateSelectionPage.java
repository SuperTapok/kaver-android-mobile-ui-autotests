package kaver.pages.onboarding;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import kaver.helpers.RandomPagesHelper;
import org.openqa.selenium.By;

import static com.codeborne.selenide.ClickOptions.usingDefaultMethod;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.appium.SelenideAppium.$;

public class BirthdayDateSelectionPage {
    private final SelenideElement
        birthDateInput    = $(By.className("android.widget.EditText")),
        nextBirthDateBtn  = $(By.id("next_-1")),
        setAdultBirthDate = $(By.id("android:id/button1"));

    private BirthdayDateSelectionPage clickBirthdayDateInput() {
        birthDateInput.click(usingDefaultMethod().force());

        return this;
    }

    private BirthdayDateSelectionPage clickAdultBirthDateBtn() {
        setAdultBirthDate.click();

        return this;
    }

    private OnboardingPage clickNextBtn() {
        nextBirthDateBtn.click();

        return new OnboardingPage();
    }

    @Step("Установить возраст 18 лет")
    public OnboardingPage setAdult() {
        if (RandomPagesHelper.isElementExist(birthDateInput, visible))
        {
            return clickBirthdayDateInput()
                .clickAdultBirthDateBtn()
                .clickNextBtn();
        }
        else {
            return new OnboardingPage();
        }
    }
}

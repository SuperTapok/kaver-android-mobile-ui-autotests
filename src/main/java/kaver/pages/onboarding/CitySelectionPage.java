package kaver.pages.onboarding;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.appium.SelenideAppium.$;

public class CitySelectionPage {
    private final SelenideElement
        kemerovoCityBtn        = $(By.id("city_1")),
        nextBtn                = $(By.id("button")),
        saintPetersburgCityBtn = $(By.id("city_62"));

    @Step("Выбрать город Кемерово")
    public CitySelectionPage clickKemerovoBtn(){
        kemerovoCityBtn.shouldBe(visible,enabled).click();

        return this;
    }

    @Step("Выбрать город СПб")
    public CitySelectionPage clickSaintPetersburgBtn() {
        saintPetersburgCityBtn.shouldBe(visible,enabled).click();

        return this;
    }

    @Step("Нажать далее")
    public GoalsPage clickNextBtn() {
        nextBtn.click();

        return new GoalsPage();
    }
}

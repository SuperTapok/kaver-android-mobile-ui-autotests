package kaver.pages.onboarding;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import kaver.pages.main.MainPage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.appium.SelenideAppium.$;

public class InfoPage {
    private final SelenideElement next5Btn = $(By.id("next_5"));

    @Step("Нажать далее на информационной страничке")
    public MainPage clickNextBtn() {
        next5Btn.click();

        return new MainPage();
    }


}

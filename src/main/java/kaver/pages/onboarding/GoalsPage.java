package kaver.pages.onboarding;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.appium.SelenideAppium.$x;

public class GoalsPage {
    private final SelenideElement seeBillboardBtn = $x("//android.widget.ScrollView/" +
            "android.view.ViewGroup/android.view.ViewGroup[2]");

    @Step("Выбрать посмотреть афишу")
    public BirthdayDateSelectionPage clickSeeBillboardsBtn() {
        seeBillboardBtn.click();

        return new BirthdayDateSelectionPage();
    }
}

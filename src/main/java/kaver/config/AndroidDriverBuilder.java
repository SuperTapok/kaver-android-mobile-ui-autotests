package kaver.config;

import com.codeborne.selenide.WebDriverRunner;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServerHasNotBeenStartedLocallyException;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.time.Duration;

public class AndroidDriverBuilder extends DriverBuilder<AndroidDriverBuilder>{
    public AndroidDriverBuilder() {
        capabilities = new DesiredCapabilities();
    }

    public AndroidDriver init() {
        AppiumDriverLocalService service = startServer();

        AndroidDriver driver = new AndroidDriver(service, this.capabilities);
        WebDriverRunner.setWebDriver(driver);
        Runtime var10000 = Runtime.getRuntime();
        AppiumDriverLocalService var10003 = service;
        var10003.getClass();
        var10000.addShutdownHook(new Thread(var10003::stop));

        return driver;
    }

    public AppiumDriverLocalService startServer() {
        AppiumDriverLocalService server = getAppiumService();
        server.start();
        if (!server.isRunning()) {
            throw new AppiumServerHasNotBeenStartedLocallyException("Appium server not started. ABORT!!!");
        }
        server.clearOutPutStreams();
        ServerManager.server.set(server);

        return server;
    }

    public AppiumDriverLocalService getAppiumService() {
        return AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
            .usingAnyFreePort()
            .withArgument(GeneralServerFlag.SESSION_OVERRIDE)
            .withTimeout(Duration.ofMinutes(10))
            .withArgument(GeneralServerFlag.ALLOW_INSECURE, "chromedriver_autodownload")
            .withArgument(GeneralServerFlag.RELAXED_SECURITY)
        );
    }

    public AndroidDriverBuilder setApp(String appPath) {
        capabilities.setCapability("appium:app", new File(appPath).getAbsolutePath());

        return this;
    }

    public AndroidDriverBuilder setDevice(String deviceName) {
        capabilities.setCapability("appium:deviceName", deviceName);

        return this;
    }

    public AndroidDriverBuilder setPlatform(Platform platform) {
        capabilities.setCapability("platformName", platform);

        return this;
    }

    public AndroidDriverBuilder setAutomation(String automation) {
        capabilities.setCapability("automationName", automation);

        return this;
    }

    public AndroidDriverBuilder autoGrantPermissions() {
        capabilities.setCapability("autoGrantPermissions", true);

        return this;
    }

    public AndroidDriverBuilder disableIdLocatorAutocompletion() {
        capabilities.setCapability("appium:disableIdLocatorAutocompletion", true);

        return this;
    }
}

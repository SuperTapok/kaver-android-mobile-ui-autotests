package kaver.conditions;

import com.codeborne.selenide.CheckResult;
import com.codeborne.selenide.Driver;
import com.codeborne.selenide.WebElementsCondition;
import org.openqa.selenium.WebElement;

import javax.annotation.Nonnull;
import java.util.List;

public class CollectionVisibleCondition extends WebElementsCondition {
    @Override
    @Nonnull
    public CheckResult check(Driver driver, List<WebElement> elements){
        return new CheckResult(true, elements.stream().allMatch(WebElement::isDisplayed));
    }

    @Override
    public String toString() {
        return null;
    }
}
